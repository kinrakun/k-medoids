# coding: utf-8
from sklearn.metrics.pairwise import pairwise_distances
import numpy as np
from numpy import array as narray
import csv
import kmedoids

data = []
nClusters = 3
with open('dataset-no-header.csv', 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in reader:
        tmp = []
        for i in row:
            tmp.append(float(i))
        data.append(narray(tmp))
    data = narray(data)

D = pairwise_distances(data, metric='euclidean') # calculate distance matrix
M, C = kmedoids.kMedoids(D, nClusters) # split into nClusters clusters

result = {}
for label in C:
    result[label] = []
    for point_idx in C[label]:
        result[label].append(data[point_idx])